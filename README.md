# Home works for ReactJS Global Mentoring Program

Each home-work is placed in separate branch.

- **Task 1**: Core concepts - `home-01`
- **Task 2**: [Webpack configuration](https://learn.epam.com/myLearning/program?groupGuid=cacdd490-bf65-4e11-81c8-6fb0ab40b619) `home-02`
- **Task 3**: [Components. Part#1](https://learn.epam.com/myLearning/program?groupGuid=cacdd490-bf65-4e11-81c8-6fb0ab40b619) `home-03`
- **Task 4**: [Components. Part#2](https://learn.epam.com/myLearning/program?groupGuid=cacdd490-bf65-4e11-81c8-6fb0ab40b619) `home-04`
- **Task 5**: [React Hooks](https://learn.epam.com/myLearning/program?groupGuid=cacdd490-bf65-4e11-81c8-6fb0ab40b619)
- **Task 6**: [Flux + Redux](https://gitlab.com/epam-donchenko/reactjs-global-mentoring-program/home-tasks/-/merge_requests/5)

## Development mode

`npm run start`

## Unit testing 

for more details visit https://jestjs.io/

`npm run test`


## Create a build

`npm run build`

