import React, {FunctionComponent, useState, useCallback} from 'react';
import styled from 'styled-components';
import Header from './components/Header';
import Main from './components/Main';
import Footer from './components/Footer';
import {AppMovieContext} from './components/context/AppContext';
import Modal from './components/common/Modal';
import MovieForm from './components/Main/MovieForm';
import {useDispatch} from 'react-redux';

import './App.css';
import Movie, {MovieProps} from "./models/movie.model";
import {createMovieAction, updateMovieAction} from "./store/movie/actions";

const AppWrapper = styled.div`
    margin-right: auto;
    margin-left: auto;
    
    @media (min-width: 1200px) {
        max-width: 1140px;
    }
`;


const App: FunctionComponent = () => {
    const dispatch = useDispatch();
    const [selected, setSelected] = useState(0);
    const [modal, setModal] = useState(false);
    const [filter, setFilter] = useState('');
    const [category, setCategory] = useState('');
    const [sort, setSortOption] = useState({key: 'yearOfRelease', value: 'release date'});

    /**
     * @desc Add movie submit
     * @param {MovieFormData}
     **/
    const onAddMovieSubmit = useCallback((props: MovieProps): void => {
        const movie: Movie = Movie.create({
            ...{
                budget: 0,
                revenue: 0
            },
            ...props
        });

        dispatch((movie.id ? updateMovieAction : createMovieAction)(movie))

        // Close dialog
        setModal(false);
    }, []);

    return (
        <AppMovieContext.Provider value={{
            selected,
            setSelected,
            modal,
            setModal,
            filter,
            setFilter,
            category,
            setCategory,
            sort,
            setSortOption
        }}>
            <AppWrapper>
                <Header/>
                <Main/>
                <Footer/>

                {modal && (
                    <Modal title={'Add movie'}
                           onClose={() => setModal(false)}>
                        <MovieForm onSubmit={onAddMovieSubmit}/>
                    </Modal>
                )}
            </AppWrapper>
        </AppMovieContext.Provider>
    );
}

export default App;