import {Constants} from './types';
import Movie, {IMovieList, MovieProps} from '../../models/movie.model';
import IFiltrableOptions from "../../components/common/Interfaces/IFiltrable";

export const deleteMovie = (movieId: number): object => ({
    type: Constants.DELETE_MOVIE,
    payload: movieId
});

export const editMovie = (movie: MovieProps): object => ({
    type: Constants.EDIT_MOVIE,
    payload: movie
});

export const loadMoviesAction: Function = (options: IFiltrableOptions) => ({
    type: Constants.LOAD_MOVIES,
    payload: options
});

export const createMovieAction = (movie: Movie): object => ({
    type: Constants.CREATE_MOVIE,
    payload: movie.toJSON()
});

export const updateMovieAction = (movie: Movie) => ({
    type: Constants.UPDATE_MOVIE,
    payload: movie
});

export const setMovieAction = (movieList: IMovieList) => ({
    type: Constants.GET_ALL_MOVIES_REQUEST_SUCCESS,
    payload: movieList
});

export const addMovieAction = (movie: Movie) => ({
    type: Constants.ADD_MOVIE,
    payload: movie
});

export const deleteMovieAction = (movieId: number) => ({
    type: Constants.DELETE_MOVIE,
    payload: movieId
});

export const putMovieErrors = (error: any) => ({
    type: Constants.GET_MOVIES_ERRORS,
    payload: error
});
