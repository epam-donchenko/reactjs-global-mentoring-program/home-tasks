import {Constants, IMovieState} from './types';
import Movie from "../../models/movie.model";

export interface MovieActions {
    type: string;
    payload: any
}

const init: IMovieState = {
    loading: false,
    list: [],
    total: 0,
    offset: 0,
    limit: 0
};

export const movieReducer = (state: IMovieState = init, action: MovieActions): IMovieState => {
    switch (action.type) {
        case Constants.DELETE_MOVIE:
            const movies = [...state.list];
            const index = movies
                .findIndex((movie:Movie) => movie.id === action.payload);

            if (index >= 0) {
                movies.splice(index, 1);
            }

            return {
                ...state,
                list: movies
            };

        case Constants.UPDATE_MOVIE:
            const mList = [...state.list];
            const mIndex = mList
                .findIndex((movie:Movie) => movie.id === action.payload.id);

            if (mIndex >= 0) {
                mList[mIndex] = action.payload;
            }

            return {
                ...state,
                list: mList
            };

        case Constants.ADD_MOVIE:
            const movie: Movie = action.payload;
            return {
                ...state,
                list: [...state.list, ...[movie]],
                total: state.total + 1
            };

        case Constants.GET_ALL_MOVIES_REQUEST_SUCCESS:
            const {limit, total, offset, list} = action.payload;

            return {
                ...state,
                limit,
                total,
                offset,
                list,
                loading: false
            };
        case Constants.LOAD_MOVIES:
            return {
                ...state,
                loading: true
            };
        case Constants.SET_LOADING:
            return {
                ...state,
                ...action.payload
            }
        default:
            return state;
    }
}

