import {Constants} from './types';
import Movie, {MovieProps} from '../../models/movie.model';
import {
    setMovieAction,
    putMovieErrors,
    addMovieAction,
    updateMovieAction,
    deleteMovieAction
} from './actions';
import {
    loadMovies,
    sendCreateMovieRequest,
    deleteMovie,
    sendUpdateMovieRequest
} from '../../services/movie.service';
import {takeEvery, takeLatest, put, all, fork} from 'redux-saga/effects';
import {IFiltrableOptions} from "../../components/common/Interfaces/IFiltrable";

interface Action {
    type: string;
    payload: any;
}

export interface ActionWithPayload<T> extends Action {
    payload: T;
}

/**
 * @desc Generator to retrieve the list of movie
 **/
export function* getAllMoviesGenerator() {
    yield takeEvery(Constants.LOAD_MOVIES, function* (action: ActionWithPayload<IFiltrableOptions>) {
        try {
            const {payload} = action;
            const movieList = yield loadMovies(payload);

            yield put(setMovieAction(movieList));
        } catch (error) {
            yield put(putMovieErrors(error));
        }
    });
}

/**
 * @desc Generator to handle the creation process
 **/
export function* createMovieGenerator() {
    yield takeLatest(Constants.CREATE_MOVIE, function* (action: ActionWithPayload<MovieProps>) {
        try {
            const {payload} = action;
            const movie: Movie = yield sendCreateMovieRequest(payload);

            yield put(addMovieAction(movie));
        } catch (error) {
            yield put(putMovieErrors(error));
        }
    })
}

/**
 * @desc Generator to handle the deletion process
 **/
export function* deleteMovieGenerator() {
    yield takeLatest(Constants.DELETE_MOVIE, function* (action: ActionWithPayload<number>) {
        try {
            const {payload: movieId} = action;
            yield deleteMovie(movieId);

            yield put(deleteMovieAction(movieId));
        } catch (error) {
            yield put(putMovieErrors(error));
        }
    })
}

/**
 * @desc Generator to handle the editing process
 **/
export function* editMovieGenerator() {
    yield takeLatest(Constants.EDIT_MOVIE, function* (action: ActionWithPayload<MovieProps>) {
        try {
            const {payload} = action;
            const movie: Movie = yield sendUpdateMovieRequest(payload);

            yield put(updateMovieAction(movie));
        } catch (error) {
            yield put(putMovieErrors(error));
        }
    });
}

// Error handling
export function* getAllMoviesError() {
    yield takeEvery(Constants.GET_MOVIES_ERRORS,
        function* () {
            console.log("error")
        })
}

export default function* rootSaga() {
    yield all([
        fork(getAllMoviesGenerator),
        fork(editMovieGenerator),
        fork(deleteMovieGenerator),
        fork(createMovieGenerator),
    ])
}