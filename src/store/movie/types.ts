import {IMovieList} from '../../models/movie.model';

export interface IMovieState extends IMovieList{
    loading: Boolean;
}

export enum Constants {
    ADD_MOVIE = 'ADD_MOVIE',
    DELETE_MOVIE = 'DELETE_MOVIE',
    LOAD_MOVIES = 'LOAD_MOVIES',
    EDIT_MOVIE = 'EDIT_MOVIE',
    CREATE_MOVIE = 'CREATE_MOVIE',

    UPDATE_MOVIE = 'UPDATE_MOVIE',
    LOAD_MOVIE_BY_ID = 'LOAD_MOVIE_BY_ID',
    GET_MOVIES_ERRORS = 'GET_MOVIES_ERRORS',
    GET_ALL_MOVIES_REQUEST_SUCCESS = 'GET_ALL_MOVIES_REQUEST_SUCCESS',
    SET_LOADING = 'SET_LOADING'
}