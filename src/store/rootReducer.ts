import {movieReducer} from './movie/reducer';

export default {
    movie: movieReducer
}