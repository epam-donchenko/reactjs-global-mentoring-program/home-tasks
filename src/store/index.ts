import {combineReducers, createStore, applyMiddleware} from 'redux';
import {IRootState} from './rootState';
import rootReducer from './rootReducer';
import rootSaga from './rootSaga';
import createSagaMiddleware from 'redux-saga';
import logger from 'redux-logger';

const sagaMiddleware = createSagaMiddleware();

const store = createStore<IRootState, any, any, any>(
    combineReducers(rootReducer),
    applyMiddleware(logger, sagaMiddleware)
);

sagaMiddleware.run(rootSaga);

export default store;