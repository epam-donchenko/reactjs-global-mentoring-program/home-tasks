import {IMovieState} from './movie/types';

export interface IRootState {
    movie: IMovieState
}
