export interface SelectItem {
    key: string;
    value: string;
}

export const sortItems: Array<SelectItem> = [
    {key: 'yearOfRelease', value: 'release_date'},
    {key: 'title', value: 'title'}
];
