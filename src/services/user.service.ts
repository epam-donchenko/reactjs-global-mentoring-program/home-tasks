export const truncateUserName = (name: string, size: number = 5) =>
    name.length <= 5
        ? name
        : `${name.substr(0, size)}...`
