import {truncateUserName} from './user.service';

test('basic', () => {
    const user = 'Alexander';
    expect(truncateUserName(user)).toBe('Alexa...');
});