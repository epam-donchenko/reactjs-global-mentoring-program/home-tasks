import Movie, {IMovieList, MovieList, MovieProps} from '../models/movie.model';
import {
    requestGET,
    requestPUT,
    requestPOST,
    requestDELETE
} from './request';
import {IFiltrableOptions} from "../components/common/Interfaces/IFiltrable";

const MOVIES_ENDPOINT: string = 'movies';

const convertFilterObjectToString = (filters: IFiltrableOptions): string => {
    let result = '';

    if (filters) {
        const list = [];
        for (const prop in filters) {
            if (filters && filters[prop]) {
                let value = filters[prop];
                if (Array.isArray(value) && value.length > 0) {
                    value = [value.join(',')];
                    list.push(`${prop}=${value}`);
                } else if (typeof value === 'string'){
                    list.push(`${prop}=${value}`);
                }
            }
        }

        if (list.length > 0) {
            result = `?${list.join('&')}`;
        }
    }

    return result;
}

/**
 * @desc Load movies
 * @return {Promise<IMovieList>}
 **/
export const loadMovies = async (filters?: IFiltrableOptions): Promise<IMovieList> => new MovieList(
    await requestGET(`${MOVIES_ENDPOINT}/${filters ? convertFilterObjectToString(filters) : ''}`),
    Movie
);

/**
 * @desc Action creator to create movie
 * @param {MovieProps} props
 * @return {Promise<Movie>}
 **/
export const sendCreateMovieRequest = async (props: MovieProps): Promise<Movie> => Movie.create(await requestPOST(MOVIES_ENDPOINT, props));

/**
 * @desc Action creator to update movie
 * @param {MovieProps} props
 * @return {Promise<Movie>}
 **/
export const sendUpdateMovieRequest = async (props: MovieProps): Promise<Movie> => Movie.create(await requestPUT(`${MOVIES_ENDPOINT}`, props));

/**
 * @desc Action creator to delete movie
 * @param {number} movieId
 * @return {Promise<any>}
 **/
export const deleteMovie = async (movieId: number): Promise<Movie> => requestDELETE(`${MOVIES_ENDPOINT}/${movieId}`);


/**
 * @desc Retrieve the movie by ID
 * @param {Number} id
 * @return Promise<Movie>
 **/
export const getMovieById = async (id: number): Promise<Movie> => Movie.create(await requestGET(`${MOVIES_ENDPOINT}/${id}`));
