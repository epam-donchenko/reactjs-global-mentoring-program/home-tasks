import axios from "axios";

const serviceURL = 'http://localhost:4000/';

interface IRequestHeader {
    [prop: string]: string;
}

interface IRequestOptions {
    headers: IRequestHeader;
}

/**
 * @desc Default headers
 **/
const defaultHeaders: IRequestHeader = {
    'content-type': 'application/x-www-form-urlencoded'
};

/**
 * @desc Build url to API service
 * @param {String} url - end-point identifier
 * @return {string}
 */
const buildUrl = (url: string) => `${serviceURL}${url}`;

/**
 * @desc GET request
 * @param {string} url
 * @param {IRequestOptions} options
 * @return {Promise}
 **/
export const requestGET = async (url: string, options?: IRequestOptions): Promise<any> => {
    const axiosOptions = options ? {
        ...options
    } : {};

    return axios
        .get(buildUrl(url), {
            headers: {
                ...defaultHeaders
            },
            ...axiosOptions
        })
        .then(response => response.data);
}

/**
 * @desc Create resource
 **/
export const requestPOST = async (url: string, data: any, options?: IRequestOptions): Promise<any> => {
    return axios
        .post(
            buildUrl(url),
            data,
            options
        )
        .then(response => response.data);
}

/**
 * @desc Create resource
 **/
export const requestPUT = async (url: string, data: any, options?: IRequestOptions): Promise<any> => {
    return axios
        .put(
            buildUrl(url),
            data,
            options
        )
        .then(response => response.data);
}

/**
 * @desc Delete resource
 **/
export const requestDELETE = async (url: string, options?: IRequestOptions): Promise<any> => {
    return axios
        .delete(
            buildUrl(url),
            options
        )
        .then(response => response.data);
}