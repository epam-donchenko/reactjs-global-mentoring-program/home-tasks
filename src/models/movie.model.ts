import BaseDAO, {IList, BaseList} from './base.dao';

export type MovieProps = {
    id?: number;
    title: string;
    overview: string; // description
    poster_path: string;
    release_date: string;
    revenue?: number;
    runtime: number; //duration
    tagline?: string; // subtitle
    vote_average?: number; //rate
    vote_count?: number;
    genres: Array<string>;
    budget?: number;
};

export default class Movie extends BaseDAO<MovieProps> {
    public constructor(props: MovieProps) {
        super(props);
    }

    public toJSON(): MovieProps {
        return {
            id: this.id,
            title: this.title,
            tagline: this.tagline,
            vote_average: this.voteAverage,
            vote_count: this.voteCount,
            release_date: new Date(this.releaseDate).toISOString(),
            poster_path: this.img,
            overview: this.overview,
            budget: this.budget * 1,
            revenue: this.revenue * 1,
            runtime: this.runtime * 1,
            genres: this.genres
        };
    }

    get id(): number {
        return this.props.id || 0;
    }

    get revenue(): number {
        return this.props.revenue || 0;
    }

    get budget(): number {
        return this.props.budget || 0;
    }

    get title(): string {
        return this.props.title;
    }

    get overview(): string {
        return this.props.overview;
    }

    get img(): string {
        return this.props.poster_path;
    }

    get tagline(): string {
        return this.props.tagline || '';
    }

    get runtime(): number {
        return this.props.runtime;
    }

    get genres(): Array<string> {
        return this.props.genres;
    }

    get voteCount(): number {
        return this.props.vote_count || 0;
    }

    get voteAverage(): number {
        return this.props.vote_average || 0;
    }

    get releaseDate(): string {
        return this.props.release_date;
    }

    get yearOfRelease(): number {
        return new Date(this.props.release_date).getFullYear();
    }

    public static create(props: MovieProps): Movie {
        return new Movie({
            ...props
        });
    }
}

export interface IMovieList extends IList<Movie> {
}

export class MovieList extends BaseList<Movie, MovieProps> {
}

export type NullableMovieModel = Movie | null;

export type ArrayOfMovies = Array<Movie>;