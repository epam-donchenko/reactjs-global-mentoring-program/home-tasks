interface Offsetability {
    offset: number;
    limit: number;
}

export interface IListResponse extends Offsetability {
    data: Array<any>;
    totalAmount: number;
}

export interface IList<T> extends Offsetability {
    list: Array<T>;
    total: number;
}

export default abstract class GenericDAO<T> {
    public readonly props: T;

    protected constructor(props: T) {
        this.props = props;
    }

    /**
     * @desc Convert object to JSON
     * @return {Object}
     **/
    public toJSON() {
        const result: any = {};

        for (const prop in this.props) {
            result[prop] = this.props[prop];
        }

        return result;
    }
}

export class BaseList<T, K> {
    public readonly props: IListResponse;
    public readonly tCreator: any;

    constructor(props: IListResponse, tCreator: new(params: K) => T) {
        this.props = props;
        this.tCreator = tCreator;
    }

    get total(): number {
        return this.props.totalAmount;
    }

    get limit(): number {
        return this.props.limit;
    }

    get offset(): number {
        return this.props.offset;
    }

    get list(): Array<T> {
        return Array.isArray(this.props.data)
            ? this.props.data.map((item: K) => new this.tCreator(item))
            : [];
    }
}