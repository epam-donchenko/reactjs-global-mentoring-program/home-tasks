import {createContext, useContext} from 'react';
import {SelectItem, sortItems} from "../../utils/sort";

export type AppMovieContextType = {
    modal: boolean; // the state of dialog
    filter: string;
    category: string;
    selected: number;
    sort: SelectItem;
    setFilter: (filter: string) => void;
    setCategory: (filter: string) => void;
    // @TODO: Replace type object with key/value interface like SelectItem but moved to upper level
    setSortOption: (option: SelectItem) => void;
    setModal: (show: boolean) => void;
    setSelected: (movieId: number) => void;
};

export const AppMovieContext = createContext<AppMovieContextType>({
    modal: false,
    selected: 0,
    filter: '',
    category: '',
    sort: sortItems[0],
    setFilter: () => console.warn('no filter provider'),
    setCategory: () => console.warn('no categories provider'),
    setSortOption: () => console.warn('no sorting logic provider'),
    setModal: () => console.warn('no modal handling logic provider'),
    setSelected: () => console.warn('no selecting logic is providing')
});

export const useAppMovieContext = () => useContext(AppMovieContext);