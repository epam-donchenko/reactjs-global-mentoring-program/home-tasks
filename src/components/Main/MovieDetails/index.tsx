import React, {FunctionComponent} from 'react';
import styled from 'styled-components';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faSearch} from '@fortawesome/free-solid-svg-icons'
import {MovieScreen} from '../../common/Movie/elements';
import MovieModel from '../../../models/movie.model';

export interface MovieDetailsProps {
    movie: MovieModel | null;
    onHide: () => void
}

const MovieDetailScreen = styled(MovieScreen)`
    width: 22%;
    height: 350px;
    padding-left: 20px;
`;

const MovieDetailsWrapper = styled.div`
    font-size: 20px;
    padding-left: 20px;
    a {
        color: #f65261;
    }
    z-index: 9;
`;

const MovieDetailsClosePanel = styled.div`
    color: #f65261;
    text-align: right;
    padding: 10px 50px 10px;
    font-size: 16px;
`;

const MovieDetailsCloseButton = styled.div`
    &:hover {
        cursor: pointer;
        color: #fff;
    }
`;

const MovieContentWrapper = styled.div`
    display: flex;
    padding-top: 20px;
`;

const ErrorPanel = styled.div`
   
`;

const MovieContent = styled.div`
    display: flex;
    padding-left: 20px;
    flex-direction: column;
    width: 70%;
`;

const MovieTitle = styled.h1`
    color: #fff;
    font-weight: 100;
    margin: 0;
    padding: 20px 0;
`;

const MovieSubTitle = styled.h2`
    color: #555;
    font-weight: 100;
    font-size: 16px;
    margin: 0;
`;

const MovieAttributes = styled.ul`
    color: #f65261;
    list-style: none;
    margin: 0;
    padding: 20px 0;
`;

const MovieDescription = styled.div`
    color: #fff;
`;

const MovieAttributeItem = styled.li`
    float: left;
    padding-right: 20px;
`;

const MovieRate = styled.span`
    color: green;
    border: 1px solid #555;
    position: relative;
    font-size: 25px;
    bottom: 6px;
    border-radius: 30px;
    padding: 15px 10px;
`;

const MovieDetails: FunctionComponent<MovieDetailsProps> = ({movie, onHide}) => {
    /**
     * @desc On close dialog close
     **/
    const onCloseButtonClick = () => {
        onHide();
    }

    if (movie === null) {
        return <ErrorPanel>The Movie has not been found</ErrorPanel>;
    }

    return (
        <MovieDetailsWrapper>
            <MovieDetailsClosePanel>
                <MovieDetailsCloseButton onClick={onCloseButtonClick}>
                    <FontAwesomeIcon icon={faSearch}/>
                </MovieDetailsCloseButton>
            </MovieDetailsClosePanel>
            <MovieContentWrapper>
                <MovieDetailScreen screen={movie.img}/>
                <MovieContent>
                    <MovieTitle>{movie.title} <MovieRate>{movie.voteAverage}</MovieRate></MovieTitle>
                    <MovieSubTitle>{movie.tagline}</MovieSubTitle>
                    <MovieAttributes>
                        <MovieAttributeItem>{movie.yearOfRelease}</MovieAttributeItem>
                        <MovieAttributeItem>{movie.runtime} min</MovieAttributeItem>
                    </MovieAttributes>
                    <MovieDescription>{movie.overview}</MovieDescription>
                </MovieContent>
            </MovieContentWrapper>
        </MovieDetailsWrapper>
    );
};

export default MovieDetails;