import React, {FunctionComponent, useEffect, useMemo} from 'react';
import styled from 'styled-components';
import Nav, {MenuItem} from './Nav';
import MoviesList from './MoviesList';
import {useAppMovieContext} from '../context/AppContext';
import {IRootState} from '../../store/rootState';
import {useDispatch, useSelector} from 'react-redux';
import {IMovieState} from '../../store/movie/types';
import {deleteMovie, editMovie, loadMoviesAction} from '../../store/movie/actions';
import {MovieProps} from "../../models/movie.model";

const menuItems: Array<MenuItem> = [
    {title: 'All', url: '#'},
    {title: 'Documentary', url: '#'},
    {title: 'Comedy', url: '#'},
    {title: 'Horror', url: '#'},
    {title: 'Crime', url: '#'}
];

const MainWrapper = styled.div`
    background-color: #232323;
    padding: 0 3em;
`;

const MovieCountWrapper = styled.div`
    padding: 20px 0;
`;

const LoadingMessage = styled.div`
    
`;

const Main: FunctionComponent = () => {
    const dispatch = useDispatch();
    const {filter: search, category: filter, sort} = useAppMovieContext();
    const {list: movies, total: totalAmount, loading} = useSelector<IRootState, IMovieState>(state => state.movie);

    useEffect(() => {
        dispatch(loadMoviesAction({
            sortBy: sort ? sort.value : '',
            sortOrder: 'asc',
            searchBy: filter ? 'genres' : 'title',
            search: search ? search : '',
            filter: filter && filter.toLowerCase() !== 'all' ? [filter] : []
        }));
    }, [filter, search, sort]);

    /**
     * @desc Delete item
     **/
    const onDeleteMovie = (movieId: number) => {
        // Dispatch delete action
        dispatch(deleteMovie(movieId));
    }

    /**
     * @desc Open dialog to edit data
     **/
    const onEditMovie = (movie: MovieProps) => {
        // Dispatch edit action
        dispatch(editMovie(movie));
    }

    return (
        <MainWrapper>
            <Nav filters={menuItems}/>

            <MovieCountWrapper>
                {
                    loading
                        ? <LoadingMessage>Loading....</LoadingMessage>
                        : <><strong>{totalAmount}</strong> movies found</>
                }
            </MovieCountWrapper>

            <MoviesList items={movies}
                        loading={loading}
                        onDeleteItem={onDeleteMovie}
                        onEditItem={onEditMovie}/>
        </MainWrapper>
    );
}

export default Main;