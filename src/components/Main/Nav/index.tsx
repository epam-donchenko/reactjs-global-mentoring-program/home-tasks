import React, {FunctionComponent} from 'react';
import styled from 'styled-components';
import Select from '../../common/Select';
import {useAppMovieContext} from '../../context/AppContext';
import {sortItems, SelectItem} from "../../../utils/sort";

export interface MenuItem {
    title: string;
    url: string;
}

export interface NavProps {
    filters: Array<MenuItem>
}

interface ItemActiveProps {
    active: boolean
}

const MainNavbarWrapper = styled.nav`
    color: #fff;    
    position: relative;
`
const ListWrapper = styled.ul`
    display: flex;
    list-style: none;
    padding: 15px 0 0;
    margin: 0;
`;
const ListItem = styled.li`
    padding: 0 10px 0 0;
    border-bottom: ${(props: ItemActiveProps) => props.active ? '3px solid #f00' : 'none'};
    &:hover {
        cursor: pointer;
    }
`;

const SeparatedLine = styled.div`
    border-bottom: 3px #555 solid;
    position:absolute;
    width: 100%;
    z-index: 0;
`;

const ManagePanelWrapper = styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    text-transform: uppercase;
    z-index: 1;
    top: 3px;
    position: relative;
`;

const Nav: FunctionComponent<NavProps> = ({filters}) => {
    const {category, setCategory, setSortOption} = useAppMovieContext();

    /**
     * @desc On sort option select
     **/
    const onSortOptionSelect = (item:SelectItem) => {
        setSortOption(item);
    }
    /**
     * @desc Set category handler
     **/
    const onCategoryClick = (item: MenuItem) => {
        setCategory(item.title);
    };

    /**
     * @desc Check if menu is selected
     * @param {MenuItem} item
     * @return {Boolean}
     **/
    const isActiveCategory = (item: MenuItem) => {
        return (!category && item.title.toLowerCase() === 'all')
            || item.title.toLowerCase() === category.toLowerCase();
    }

    return (
        <MainNavbarWrapper>
            <ManagePanelWrapper>
                <ListWrapper>
                    {filters?.map((item: MenuItem) => (
                        <ListItem key={item.title}
                                  active={isActiveCategory(item)}
                                  onClick={() => onCategoryClick(item)}>{item.title}</ListItem>
                    ))}
                </ListWrapper>
                <Select label={'Sort by'}
                        onSelect={onSortOptionSelect}
                        selected={sortItems[0]}
                        items={sortItems}/>
            </ManagePanelWrapper>
            <SeparatedLine/>
        </MainNavbarWrapper>
    );
}

export default Nav;