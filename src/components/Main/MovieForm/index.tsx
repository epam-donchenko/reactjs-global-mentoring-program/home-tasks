import React, {FunctionComponent, useState, useMemo} from 'react';
import styled from 'styled-components';
import {FieldLabel, FieldText, FieldSelect, FieldWrapper} from '../../common/Form/Field';
import {PrimaryButton, SecondaryButton, ButtonsWrapper} from "../../common/Buttons";
import FieldDatePicker from '../../common/DatePicker';
import Movie, {MovieProps} from "../../../models/movie.model";
import {IFormable} from "../../common/Interfaces/fomable";

const genreItems = [
    'Action',
    'Animation',
    'Fantasy',
    'Adventure',
    'Science Fiction',
    'Family',
    'Comedy',
    'Drama',
    'Romance'
];

export interface MovieFormProps {
    movie?: Movie,
    onSubmit: (data: MovieProps) => void
}

export interface MovieFormData {
    title: string,
    poster_path: string,
    overview: string,
    runtime: string
}

const MovieFormWrapper = styled.form<IFormable>`
    font-size: 20px;
    padding-left: 20px;
 
    a {
        color: #f65261;
    }
    z-index: 9;
`;

const SubmitButton = styled(PrimaryButton)`
    min-width: 150px;
`;

const ResetButton = styled(SecondaryButton)`
    min-width: 150px;
`;

const MovieForm: FunctionComponent<MovieFormProps> = ({onSubmit, movie}) => {
    const initFormData: MovieProps = useMemo(() => movie
        ? movie.toJSON()
        : {
            title: '',
            poster_path: '',
            overview: '',
            release_date: new Date().toISOString(),
            genres: [],
            runtime: 0
        }, []);
    const [form, setForm] = useState(initFormData);

    /**
     * @desc Handle change event for the form
     **/
    const onChangeInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const {name, value} = e.target;

        setForm(state => {
            return {
                ...state,
                [name]: value
            };
        })
    }

    const onSelectDate = (date: any) => {
        const d = new Date(date);

        setForm(state => {
            return {
                ...state,
                release_date: d.toISOString()//`${d.getFullYear()}-${d.getMonth()}-${d.getDay()}`
            };
        });
    }

    /**
     * @desc Select genre
     **/
    const onSelectGenre = (e: React.ChangeEvent<HTMLSelectElement>) => {
        const value = e.target.value === '0'
            ? []
            : Array.from(e.target.selectedOptions, option => option.value);

        setForm(state => {
            return {
                ...state,
                genres: value
            };
        });
    }

    /**
     * @desc Handle the submit
     **/
    const onMovieSubmit = (e: React.FormEvent<HTMLInputElement>) => {
        e.preventDefault();
        e.stopPropagation();
        onSubmit(form);
    }

    return (
        <MovieFormWrapper onSubmit={onMovieSubmit}>
            <FieldWrapper>
                <FieldLabel>Title</FieldLabel>
                <FieldText name="title"
                           value={form.title}
                           onChange={onChangeInput}
                           placeholder={'The name of the movie'}
                           required/>
            </FieldWrapper>

            <FieldWrapper>
                <FieldLabel>Release date</FieldLabel>
                <FieldDatePicker onSelect={onSelectDate}
                                 selected={new Date(form.release_date)}
                                 required={true}/>
            </FieldWrapper>

            <FieldWrapper>
                <FieldLabel>Movie URL</FieldLabel>
                <FieldText name="poster_path"
                           value={form.poster_path}
                           onChange={onChangeInput}
                           placeholder={'Movie URL here'}
                           required/>
            </FieldWrapper>

            <FieldWrapper>
                <FieldLabel>Genre</FieldLabel>
                <FieldSelect onChange={onSelectGenre}
                             name='genres[]'
                             value={form.genres}
                             multiple>
                    {genreItems.map((genre: string) => <option
                        key={genre} value={genre}>{genre}</option>)}
                </FieldSelect>
            </FieldWrapper>

            <FieldWrapper>
                <FieldLabel>Overview</FieldLabel>
                <FieldText name="overview"
                           value={form.overview}
                           onChange={onChangeInput}
                           placeholder={'Overview here'}
                           required/>
            </FieldWrapper>

            <FieldWrapper>
                <FieldLabel>Runtime</FieldLabel>
                <FieldText name="runtime"
                           value={form.runtime}
                           onChange={onChangeInput}
                           placeholder={'Runtime here'}
                           required/>
            </FieldWrapper>

            <ButtonsWrapper>
                <ResetButton type="reset">Reset</ResetButton>
                <SubmitButton type="submit">{form.id ? 'Update' : 'Submit'}</SubmitButton>
            </ButtonsWrapper>
        </MovieFormWrapper>
    );
};

export default MovieForm;