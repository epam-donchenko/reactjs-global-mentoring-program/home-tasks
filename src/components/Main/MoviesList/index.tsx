import React, {FunctionComponent, useCallback, useState} from 'react';
import styled from 'styled-components';
import MovieItem from './MovieItem';
import MovieModel, {MovieProps} from '../../../models/movie.model';
import ErrorBoundary from './ErrorBoundary';
import {useAppMovieContext} from "../../context/AppContext";
import Modal from '../../common/Modal';
import Movie from '../../../models/movie.model';
import MovieForm from '../MovieForm';
import {
    ButtonsWrapper,
    PrimaryButton,
    SecondaryButton
} from '../../common/Buttons';

const CancelButton = styled(SecondaryButton)`
    min-width: 150px;
`;

const SubmitButton = styled(PrimaryButton)`
    min-width: 150px;
`;

const DeleteTextWrapper = styled.div`
`;

const MoviesWrapper = styled.div`
    display: flex;
    flex-wrap: wrap;
    gap: 3em;
    padding: 0 0 30px 0;
    justify-content: space-between;
    
    & > div {
       flex-grow: 0;
       flex-shrink: 0;
       width: 30%; 
    }
    
    
    @media (max-width: 1050px) {
       gap: 1em;
       
       & > div {
        width: 25%;
       }
    }  
`;

export interface MovieListProps {
    items: Array<MovieModel>;
    loading: boolean;
    onDeleteItem: (movieId: number) => void;
    onEditItem: (movie: MovieProps) => void;
}

const MoviesList: FunctionComponent<MovieListProps> = ({items, onDeleteItem, onEditItem, loading= false}) => {
    const {setSelected, selected} = useAppMovieContext();
    const [showDeleteDialog, setShowDeleteDialog] = useState<number>(0);
    const [editedMovie, setEditedMovie] = useState<Movie | null>(null);

    /**
     * @desc Close confirmation dialog
     **/
    const onDeleteConfirmationDialogClose = () => {
        setShowDeleteDialog(0);
    }

    /**
     * @desc On movie click handler
     **/
    const onMovieItemSelect = useCallback((movieId: number) => {
        setSelected(movieId);
    }, [selected]);

    /**
     * Open edit dialog
     * @param {Movie} movie
     **/
    const onMovieItemEditClick = (movie: Movie) => {
        setEditedMovie(movie);
    };

    const closeEditModal = useCallback(() : void => {
        setEditedMovie(null);
    }, []);
    /**
     * @desc On submit edit form
     **/
    const onEditMovieSubmit = useCallback((props: MovieProps): void => {
        onEditItem(props);

        // close modal dialog
        closeEditModal();
    }, []);

    /**
     *
     **/
    const onMovieItemDeleteClick = (movieId: number) => {
        setShowDeleteDialog(movieId);
    };

    /**
     * @desc Dispatching the delete action
     **/
    const onDeleteButtonClick = () => {
        // dispatch delete action
        onDeleteItem(showDeleteDialog);

        // Close dialog
        onDeleteConfirmationDialogClose()
    }

    return (
        <ErrorBoundary>
            <MoviesWrapper className={loading ? 'highlight-wrapper' : ''}>
                {
                    items.map((movie: MovieModel) => (
                        <MovieItem key={movie.id}
                                   onSelect={onMovieItemSelect}
                                   onEditItem={onMovieItemEditClick}
                                   onDeleteItem={onMovieItemDeleteClick}
                                   item={movie}/>
                    ))
                }
            </MoviesWrapper>

            {!!showDeleteDialog && (
                <Modal title={'Delete Item'}
                       onClose={onDeleteConfirmationDialogClose}>
                    <DeleteTextWrapper>Are you sure you want to delete selected item?</DeleteTextWrapper>
                    <ButtonsWrapper>
                        <CancelButton type="button"
                                      onClick={onDeleteConfirmationDialogClose}> Cancel</CancelButton>
                        <SubmitButton type="button"
                                      onClick={onDeleteButtonClick}> Delete</SubmitButton>
                    </ButtonsWrapper>
                </Modal>
            )}

            {editedMovie && (
                <Modal title={`Edit movie #${editedMovie.id}`}
                       onClose={closeEditModal}>
                    <MovieForm movie={editedMovie}
                               onSubmit={onEditMovieSubmit}/>
                </Modal>
            )}
        </ErrorBoundary>
    );
}

export default MoviesList;