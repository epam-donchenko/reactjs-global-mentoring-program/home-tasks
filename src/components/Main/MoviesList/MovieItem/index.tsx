import React, {FunctionComponent, useCallback, useState} from 'react';
import styled from 'styled-components';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faEllipsisV, faTimes} from '@fortawesome/free-solid-svg-icons'
import {MovieScreen} from '../../../common/Movie/elements';
import MovieModel from '../../../../models/movie.model';
import {IClickable} from '../../../common/Interfaces/IClickable';
import Movie from '../../../../models/movie.model';

export type MovieItemProps = {
    item: MovieModel;
    onSelect: (movieId: number) => void;
    onEditItem: (movie: Movie) => void;
    onDeleteItem: (movieId: number) => void;
}

interface ManageMenuProps {
    opened: boolean;
}

interface IMenuItemCloseButton extends IClickable {
    href: string;
}

const MovieItemWrapper = styled.div`
    color: #555;
    position: relative;
    z-index: 0;
    
    & > .manage-badge {
       display: none;
    }
    
    
    &:hover {
        cursor: pointer;
        
        & > .manage-badge {
            display: block;
        }
    }
`;

const MovieManageBadge = styled.div<IClickable>`
    position: absolute;
    background: #232323;
    border-radius: 15px;
    color: #fff;
    font-size: 12px;
    height: 18px;
    padding: 5px;
    text-align: center;
    width: 18px;
    right: 10px;
    top: 10px;
    
    &:hover {
        cursor: pointer;
    }
    
    z-index: 1;
`;

const MovieTitleWrapper = styled.div`
    display: flex;
    justify-content: space-between;
    padding: 10px 0;
`;

const MovieTitle = styled.h1`
    font-size: 18px;
    margin:0;
    font-weight: 200;
`;

const MovieYearWrapper = styled.div`
    padding: 5px;
`;

const MovieYear = styled.div`
    border: 1px #555 solid;
    border-radius: 3px;
    padding: 2px 0;
    text-align: center;
    width: 70px;
`;

const MovieGenre = styled.div`
    font-size: 14px;
`;

const MenuWrapper = styled.ul`
    margin: 0;
    padding: 0 0 10px 0;
    border-radius: 3px;
    position: absolute;
    background: #232323;
    width: 50%;
    top: 10px;
    right: 10px;
    list-style: none;
    display: ${(props:ManageMenuProps) => props.opened ? 'block': 'none'};
    z-index: 100;
`;

const MenuItem = styled.li<IClickable>`
   text-align: left;
   padding: 5px;
   &:hover {
      color: #fff;
      cursor: pointer;
   }
`;

const MenuCloseItem = styled.li`
   text-align: right;
   padding: 5px 10px;
`;

const DangerMenuItem = styled(MenuItem)<IClickable>`
    &:hover {
        background-color: #F65261;    
    }
`;

const MenuItemCloseButton = styled.a<IMenuItemCloseButton>`
    color: #fff;  
`;

const MovieItem: FunctionComponent<MovieItemProps> = ({item, onSelect, onDeleteItem, onEditItem}) => {
    const [showMenu, setShowMenu] = useState(false);

    /**
     * @desc Handling the item click
     **/
    const onItemClick = useCallback(() => {
        onSelect(item.id);
    }, [item]);

    /**
     * @desc Delete movie item
     * @param {MouseEvent} e
     **/
    const onDeleteMovie = (e: MouseEvent) => {
        e.stopPropagation();
        setShowMenu(false);
        onDeleteItem(item.id);
    }

    /**
     * @desc Delete movie item
     * @param {MouseEvent} e
     **/
    const onEditMovie = (e: MouseEvent) => {
        e.stopPropagation();
        setShowMenu(false);
        onEditItem(item);
    }

    /**
     * @desc Open menu for current movie item
     * @param {MouseEvent} e
     **/
    const onManageBadgeClick = (e: MouseEvent) => {
        e.stopPropagation();
        setShowMenu(true);
    }

    /**
     * @desc Handle menu close button click
     * @param {MouseEvent} e
     **/
    const onMenuCloseButtonClick = (e: MouseEvent) => {
        e.stopPropagation();
        e.preventDefault();
        setShowMenu(false);
    }

    return (
        <MovieItemWrapper onClick={onItemClick}>
            <MovieManageBadge className="manage-badge"
                              onClick={onManageBadgeClick}>
                <FontAwesomeIcon icon={faEllipsisV} />
            </MovieManageBadge>
            <MenuWrapper opened={showMenu}>
                <MenuCloseItem>
                    <MenuItemCloseButton href=""
                                         onClick={onMenuCloseButtonClick}>
                        <FontAwesomeIcon icon={faTimes} />
                    </MenuItemCloseButton>
                </MenuCloseItem>
                <MenuItem onClick={onEditMovie}>Edit</MenuItem>
                <DangerMenuItem
                    onClick={onDeleteMovie}>Delete</DangerMenuItem>
            </MenuWrapper>
            <MovieScreen screen={item.img}/>
            <MovieTitleWrapper >
                <MovieTitle>{item.title}</MovieTitle>
                <MovieYearWrapper>
                    <MovieYear>{item.yearOfRelease}</MovieYear>
                </MovieYearWrapper>
            </MovieTitleWrapper>
            <MovieGenre>{ item.genres.join(', ') }</MovieGenre>
        </MovieItemWrapper>
    );
}

export default MovieItem;