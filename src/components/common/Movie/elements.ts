import styled from 'styled-components';

interface MovieItemScreenProps {
    screen: string
}

export const MovieScreen = styled.div`
    background: url(${(props:  MovieItemScreenProps) => props.screen}) 100% 100% no-repeat;
    background-size: contain;
   height: 31vw;
`;
