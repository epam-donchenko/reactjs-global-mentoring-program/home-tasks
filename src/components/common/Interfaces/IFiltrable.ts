export interface IFiltrableOptions {
    sortBy: string;
    sortOrder?: string;
    search: string;
    searchBy: string;
    filter: Array<string>;
    offset?:'',
    limit?: ''
}