export interface IClickable {
    onClick: Function
}