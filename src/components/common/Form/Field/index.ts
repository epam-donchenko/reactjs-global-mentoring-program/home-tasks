import styled from 'styled-components';

export const FieldWrapper = styled.div`
    display: flex;
    flex-direction: column;
    padding: 10px 0;
`;

export const FieldLabel = styled.label`
    color: #F65261;
    padding-bottom: 10px;
    text-transform: uppercase;
`;

export const FieldText = styled.input.attrs({
    type: 'text'
})`
    background-color: #424242;
    border: none;
    border-radius: 3px;
    color: #fff;
    font-size: 20px;
    padding: 10px;
`;

export const FieldSelect = styled.select`
    background-color: #424242;
    border: none;
    border-radius: 3px;
    color: #fff;
    font-size: 20px;
    padding: 10px;
    
    -webkit-appearance: none;
    -moz-appearance: none;
   
    background-image: url("data:image/svg+xml;utf8,<svg fill='lightcoral' height='30' viewBox='0 0 30 30' width='30' xmlns='http://www.w3.org/2000/svg'><path d='M7 10l5 5 5-5z'/><path d='M0 0h30v30H0z' fill='none'/></svg>");
     background-repeat: no-repeat;
    background-position-x: 99%;
    background-position-y: 10px;
`;

