import React, {FunctionComponent, useState} from 'react';
import styled from 'styled-components';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faCalendarAlt} from '@fortawesome/free-solid-svg-icons';
import DatePicker from 'react-date-picker'

export interface PickerProps {
    selected?: Date;
    onSelect: Function;
    required: boolean
}

const DatePickerWrapper = styled.div`
    display: flex;
    
    & > .custom-date-picker {
        width: 100%;
        
        & > .react-date-picker__wrapper {
            background-color: #424242;
            border-radius: 3px;
            padding: 5px;
            
            .react-date-picker__clear-button {
                color: #fff;
            }
            
            .react-date-picker__inputGroup__input{
               color: #fff;
            }
            
            .react-date-picker__button {
                color: #F65261;
                font-size: 20px;
            }
        }
    }
`;

const DatePickerCustom: FunctionComponent<PickerProps> = ({
                                                              selected,
                                                              onSelect = () => {
                                                              },
                                                              required = false
                                                          }) => {
    const [value, onChange] = useState(selected);

    /**
     * @desc Handle event
     **/
    const onChangeDatePicker = (val: any) => {
        onChange(val);
        onSelect(val);
    }

    return (
        <DatePickerWrapper>
            <DatePicker className='custom-date-picker'
                        onChange={onChangeDatePicker}
                        format='dd/MM/y'
                        value={value}
                        required={required}
                        calendarIcon={<FontAwesomeIcon icon={faCalendarAlt}/>}/>
        </DatePickerWrapper>
    );
};

export default DatePickerCustom;