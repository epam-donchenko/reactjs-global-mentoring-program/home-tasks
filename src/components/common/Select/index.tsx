import React, {FunctionComponent, useState} from 'react';
import styled from 'styled-components';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faCaretDown, faCaretUp} from '@fortawesome/free-solid-svg-icons'
import {SelectItem} from '../../../utils/sort';

interface ElementStatusProps {
    status: boolean
}

export interface SelectProps {
    label: string;
    items: Array<SelectItem>;
    selected?: SelectItem;
    onSelect: Function
}

const SelectWrapper = styled.div`
    display: flex;
    padding: 15px 0;
    gap: 1.5em;
    position: relative;
    label {
        color: #555;
    }
    cursor: pointer;
    .selector-caret {
        color: #f65261;
    }
`;

const SelectedOption = styled.div`
    width: 150px;
`;

const SelectListWrapper = styled.ul`
    display: ${(props: ElementStatusProps) => props.status ? 'block' : 'none'};
    background: #555;
    border-radius: 3px;
    list-style: none;
    left: 80px;
    padding: 10px;
    position: absolute;
    top: 25px;
    width: 180px;
`;

const SelectListItem = styled.li`
    padding: 3px;
    
    &:hover {
        background: #424242;
    }
`;

const Select: FunctionComponent<SelectProps> = ({
                                                    label = 'label',
                                                    items,
                                                    selected: selectedValue,
                                                    onSelect = () => {}
                                                }) => {
    const [opened, setOpened] = useState(false);
    const [selected, setSelected] = useState(selectedValue);

    /**
     * @desc Click handler
     **/
    const onSelectorClick = () => {
        setOpened((opened: boolean) => !opened);
    };

    /**
     * @desc
     **/
    const onItemSelect = (item: SelectItem) => {
        setSelected(item);
        onSelect(item);
    };

    return (
        <SelectWrapper onClick={onSelectorClick}>
            <label>{label}</label>

            {selected
                ? <SelectedOption>{selected.value}</SelectedOption>
                : null
            }

            <FontAwesomeIcon icon={opened ? faCaretUp : faCaretDown}
                             className='selector-caret'/>

            <SelectListWrapper status={opened}>
                {
                    items
                        .filter((item: SelectItem) => item.key !== selected?.key)
                        .map((item: SelectItem) => (
                            <SelectListItem key={item.key}
                                            onClick={() => onItemSelect(item)}>
                                {item.value}
                            </SelectListItem>
                        ))
                }
            </SelectListWrapper>
        </SelectWrapper>
    );
}

export default Select;