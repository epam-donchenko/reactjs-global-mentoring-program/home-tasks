import styled from 'styled-components';

const Button = styled.button`
    border: none;
    border-radius: 3px;
    font-size: 20px;
    padding: 13px;
    min-width: 120px;
    text-transform: uppercase;
    
    &:hover {
        cursor: pointer;
    }
    
    &:focus {
        outline-width: 0;
    }
`;

export const PrimaryButton = styled(Button)`
    background-color: #F65261;
    color: #fff;
    
    &:hover {
        background-color: #F6627B;
    }
`;

export const SecondaryButton = styled(Button)`
    color: #F65261;
    background-color: transparent;
    border: 1px #F65261 solid;
`;

export const ButtonsWrapper = styled.div`
    display: flex;
    justify-content: flex-end;
    gap: 10px;
    padding-top: 40px;
`;

