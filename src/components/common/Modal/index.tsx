import React, {FunctionComponent} from 'react';
import styled from 'styled-components';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faTimes} from '@fortawesome/free-solid-svg-icons'
import {IClickable} from '../Interfaces/IClickable';

interface ICloseButton extends IClickable {
    href: string;
}

export interface ModalProps {
    children?: React.ReactNode,
    title: string,
    onClose?: Function
}

const ModalWrapper = styled.div`
    background: #232323;
    border-radius: 5px;
    padding: 20px;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    z-index: 1;
    width: 500px;
    height: 100%;
    overflow-y: scroll;
`;

const Overlay = styled.div`
  position: fixed;
  backdrop-filter: blur(5px);
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  background: rgba(0, 0, 0, 0.75);
  z-index: 4;
`;

const ModalCloseButtonWrapper = styled.div`
    text-align: right;
`;

const ModalTitle = styled.h1`
    text-transform: uppercase;
    font-weight: 100;
`;

const CloseButton = styled.a<ICloseButton>`
    color: #fff;
    font-size: 20px;
`;


const ModalContentWrapper = styled.div`
`;

/**
 * @desc Modal component
 **/
const Modal: FunctionComponent<ModalProps> = ({
                                                  children,
                                                  title,
                                                  onClose = (): void => {
                                                  }
                                              }) => {
    /**
     * @desc Close dialog
     * @param {MouseEvent} e
     **/
    const onCloseButtonClick = (e: MouseEvent) => {
        e.preventDefault();

        if (onClose) {
            onClose();
        }
    };

    return (
        <Overlay>
            <ModalWrapper>
                <ModalCloseButtonWrapper>
                    <CloseButton href="#"
                                 onClick={onCloseButtonClick}>
                        <FontAwesomeIcon icon={faTimes}/>
                    </CloseButton>
                </ModalCloseButtonWrapper>
                <ModalTitle>{title}</ModalTitle>
                <ModalContentWrapper>{children}</ModalContentWrapper>
            </ModalWrapper>
        </Overlay>
    );
};

export default Modal;
