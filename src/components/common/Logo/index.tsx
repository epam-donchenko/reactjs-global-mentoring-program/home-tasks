import React, {FunctionComponent} from 'react';
import styled from 'styled-components';

export interface LogoProps {

}

const LogoWrapper = styled.div`
    font-size: 20px;
    padding-left: 20px;
    a {
        color: #f65261;
    }
    z-index: 9;
`;

const Logo: FunctionComponent<LogoProps> = ({}) => {
    return (
        <LogoWrapper>
            <a href="/"><strong>netflix</strong>roulette</a>
        </LogoWrapper>
    );
};

export default Logo;