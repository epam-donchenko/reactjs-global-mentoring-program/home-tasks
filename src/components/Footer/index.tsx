import React, {FunctionComponent} from 'react';
import styled from 'styled-components';
import Logo from '../common/Logo';

const FooterWrapper = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    background: #424242;
    height: 80px;
`;

const Footer: FunctionComponent = () => {
    return (
        <FooterWrapper>
            <Logo/>
        </FooterWrapper>
    );
};

export default Footer;