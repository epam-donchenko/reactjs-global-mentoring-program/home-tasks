import {useEffect} from 'react';

/**
 * @desc use custom hook
 **/
export const useEffectAsync = (effect: Function, inputs: Array<any>) => {
    useEffect(() => {
        effect();
    }, inputs);
}