import {useEffect, useState} from 'react';

type Behavior = 'smooth' | 'auto' | undefined;

interface ScrollOptions {
    left: number;
    top: number;
    behavior: Behavior;
}

/**
 * @desc Custom hook to make the scroll
 **/
export const useScroll = (dependencies: Array<any> = [], options?: ScrollOptions) => {
    const [option, setOption] = useState<ScrollOptions>(options || {
        left: 0,
        top: 0,
        behavior: 'smooth'
    })
    const {left, top, behavior} = option;

    useEffect(() => {
        window.scrollTo({
            left,
            top,
            behavior
        })
    }, dependencies);

    return [option, setOption]
}