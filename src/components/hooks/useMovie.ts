import {useState} from 'react';
import {NullableMovieModel} from '../../models/movie.model';
import {getMovieById} from '../../services/movie.service';
import {useEffectAsync} from './useEffectAsync';

export const useMovie = (id: number) => {
    const [movie, setMovie] = useState<NullableMovieModel>(null);

    useEffectAsync(async () => {
        if (id > 0) {
            setMovie(await getMovieById(id));
        }
    }, [id]);

    return [movie, setMovie] as const;
}