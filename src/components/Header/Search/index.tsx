import React, {useState, ChangeEvent, FunctionComponent} from 'react';
import styled from 'styled-components';
import {PrimaryButton} from '../../common/Buttons';

export interface SearchProps {
    onSubmit: Function;
    placeholder: string;
}

const SearchWrapper = styled.div`
    padding: 20px 10%;
`;

const SearchTitle = styled.h1`
    color: #fff;
    font-size: 30px;
    font-weight: 100;
    text-transform: uppercase;
`;

const SearchFieldWrapper = styled.div`
    display: flex;
    gap: 0.35em;
`;

const SearchInput = styled.input`
    background-color: #424242;
    border: none;
    border-radius: 3px;
    color: #fff;
    font-size: 25px;
    flex: 3 1 auto;
    min-width: 320px;
    padding: 10px;
`;

const SearchButton = styled(PrimaryButton)`
    flex: 1 1 100px;
`;

const Search: FunctionComponent<SearchProps> = ({onSubmit, placeholder}) => {
    const [search, setSearch] = useState('');

    /**
     * @desc Handle the input change event
     **/
    const onSearchInputChange = (e: ChangeEvent<HTMLInputElement>): void => {
        setSearch(e.currentTarget.value);
    }

    /**
     * @desc Handle the form submit event
     * @param {React.SyntheticEvent} e
     * @return {void}
     **/
    const onSearchFrmSubmit = (e: React.SyntheticEvent): void => {
        e.preventDefault();

        onSubmit(search);
    };

    return (
        <SearchWrapper>
            <SearchTitle>Find your movie</SearchTitle>
            <form onSubmit={onSearchFrmSubmit}>
                <SearchFieldWrapper>
                    <SearchInput onChange={onSearchInputChange}
                                 value={search}
                                 placeholder={placeholder}/>
                    <SearchButton type={'submit'}>Search</SearchButton>
                </SearchFieldWrapper>
            </form>
        </SearchWrapper>
    );
}

export default Search;