import React, {FunctionComponent, useCallback} from 'react';
import styled from 'styled-components';
import TopNav from './TopNav';
import Search from './Search';
import {useAppMovieContext} from '../context/AppContext';
import MovieDetails from '../Main/MovieDetails';
import {useMovie} from '../hooks/useMovie';
import img from '../../assets/bg_header.jpg';
import {useScroll} from "../hooks/useScroll";

const HeaderWrapper = styled.header`
    padding: 10px 20px;
    margin-bottom: 10px;
    min-height: 430px;
    position: relative;
    z-index: 4;
`;

const HeaderBackGround = styled.div`
    position: absolute;
    z-index: 1;
    left: 0;
    top: 0;
    right: 0;
    bottom: 0;
    filter: blur(2px);
    background: url(${img}) no-repeat center #000;
    background-size: cover;
    
`;

const HeaderElementsWrapper = styled.div`
    position: relative;
    z-index: 2;
`;

const HeaderMovieDetails = styled.div`
    backdrop-filter: blur(5px);
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    background: rgba(0, 0, 0, 0.75);
    z-index: 7;
    position: absolute;
`;

const Header: FunctionComponent = () => {
    const {setFilter, selected, setSelected} = useAppMovieContext();
    const [movie] = useMovie(selected);
    const [] = useScroll([selected])

    /**
     * @desc On hide details panel
     **/
    const onDetailsPanelHide = useCallback(() => {
        setSelected(0);
    }, [selected]);

    /**
     * @desc Handle the submit search form
     **/
    const onSearchSubmit = (value: string) => {
        setFilter(value);
    };

    return (
        <HeaderWrapper>
            <HeaderBackGround/>
            <TopNav/>
            <HeaderElementsWrapper>
                <Search onSubmit={onSearchSubmit}
                        placeholder={'What do you want to watch?'}/>
            </HeaderElementsWrapper>
            {selected
                ?
                <HeaderMovieDetails>
                    <MovieDetails movie={movie}
                                  onHide={onDetailsPanelHide}/>
                </HeaderMovieDetails>
                : null
            }
        </HeaderWrapper>
    );
}

export default Header;