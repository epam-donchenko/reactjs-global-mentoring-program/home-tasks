import React, {FunctionComponent} from 'react';
import styled from 'styled-components';
import Logo from '../../common/Logo';
import {useAppMovieContext} from '../../context/AppContext';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faPlus} from '@fortawesome/free-solid-svg-icons'

const TopNavWrapper = styled.div`
    display: flex;
    color: #F65261;
    justify-content: space-between;
    width: 100%;
`;

const AddMovieButton = styled.button`
    color: inherit;
    border: none;
    border-radius: 3px;
    background-color: #555;
    font-weight: 600;
    opacity: 0.7;
    padding: 10px;
    text-transform: uppercase;
    z-index: 4;
    
    &:hover {
        cursor: pointer;
        opacity: 0.95;
    }
    &:focus {
        outline-width: 0;
    }
`;

const TopNav: FunctionComponent = () => {
    const addMovieText = 'Add Movie';
    const {setModal} = useAppMovieContext();

    /**
     * @desc Show the dialog
     **/
    const onAddMovieBtnClick = () => {
        setModal(true);
    }

    return (
        <TopNavWrapper>
            <Logo/>
            <AddMovieButton onClick={onAddMovieBtnClick}>
                <FontAwesomeIcon icon={faPlus} /> {addMovieText}
            </AddMovieButton>
        </TopNavWrapper>
    );
};

export default TopNav;