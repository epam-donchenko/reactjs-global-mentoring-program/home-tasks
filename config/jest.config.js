module.exports = {
	'verbose': true,
	'roots': [
		'../src'
	],
	'transform': {
		'.*\.tsx?$': 'ts-jest'
	},
	'testRegex': '(/__tests__/.*|(\\.|/)(test|spec))\\.(jsx?|tsx?)$',
	'moduleFileExtensions': [
		'ts',
		'tsx',
		'js',
		'jsx',
		'json',
		'node'
	],
	'moduleNameMapper': {
		'\.(css|jpg|png)$': './config/empty-module.js',
	},
};